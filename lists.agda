{-
  This editor runs on agdapad.quasicoherent.io. Your Agda code is stored on
  this server and should be available when you revisit the same Agdapad session.
  However, absolutely no guarantees are made. You should make backups by
  downloading (see the clipboard icon in the lower right corner).

  C-c C-l          check file
  C-c C-SPC        check hole
  C-c C-,          display goal and context
  C-c C-c          split cases
  C-c C-r          fill in boilerplate from goal
  C-c C-d          display type of expression
  C-c C-v          evaluate expression (normally this is C-c C-n)
  C-c C-a          try to find proof automatically
  C-z              enable Vi keybindings
  C-x C-+          increase font size
  \bN \alpha \to   math symbols

  "C-c" means "<Ctrl key> + c". In case your browser is intercepting C-c,
  you can also use C-u. For pasting code into the Agdapad, see the clipboard
  icon in the lower right corner.

  In text mode, use <F10> to access the menu bar, not the mouse.
-}

data Void : Set where

Not : (t : Set) -> Set
Not t = t -> Void

data Val : Set where
  foo : Val
  bar : Val
  baz : Val

data List : Set where
  nil : List
  cons : Val -> List -> List


data IsElemOf : Val -> List -> Set where
  here : {val : Val} -> {list : List} -> IsElemOf val (cons val list)
  there : {x val : Val} -> {list : List} -> IsElemOf val list -> IsElemOf val (cons x list)


-------

example : List
example = cons foo (cons bar nil)

prf1 : IsElemOf foo example
prf1 = here

prf2 : IsElemOf bar example
prf2 = there here

prf3 : Not (IsElemOf baz example)
prf3 (there (there ()))


-------

data Maybe (T : Set) : Set where
  Nothing : Maybe T
  Just : T -> Maybe T

mapMaybe : {T1 T2 : Set} -> (T1 -> T2) -> Maybe T1 -> Maybe T2
mapMaybe fn Nothing = Nothing
mapMaybe fn (Just x) = Just (fn x)


data Equal : Val -> Val -> Set where
  eql : {x : Val} -> Equal x x

checkEql : (a : Val) -> (b : Val) -> Maybe (Equal a b)
checkEql foo foo = Just eql
checkEql foo bar = Nothing
checkEql foo baz = Nothing
checkEql bar foo = Nothing
checkEql bar bar = Just eql
checkEql bar baz = Nothing
checkEql baz foo = Nothing
checkEql baz bar = Nothing
checkEql baz baz = Just eql


isElem : (val : Val) -> (list : List) -> Maybe (IsElemOf val list)
isElem val nil = Nothing
isElem val (cons x list) = case (checkEql val x)
  where
  case : Maybe (Equal val x) -> Maybe (IsElemOf val (cons x list))
  case Nothing = mapMaybe there (isElem val list)
  case (Just eql) = Just here


-------

notThere : {val x : Val} -> {list : List} -> Not (Equal val x) -> Not (IsElemOf val list) -> Not (IsElemOf val (cons x list))
notThere p1 p2 here = p1 eql
notThere p1 p2 (there pp) = p2 pp


data Dec (T : Set) : Set where
  yes : T -> Dec T
  no : Not T -> Dec T

mapDec : {A B : Set} -> (yesFn : A -> B) -> (noFn : Not A -> Not B) -> Dec A -> Dec B
mapDec yesFn noFn (yes x) = yes (yesFn x)
mapDec yesFn noFn (no x) = no (noFn x)

decideEql : (a b : Val) -> Dec (Equal a b)
decideEql foo foo = yes eql
decideEql foo bar = no (λ ())
decideEql foo baz = no (λ ())
decideEql bar foo = no (λ ())
decideEql bar bar = yes eql
decideEql bar baz = no (λ ())
decideEql baz foo = no (λ ())
decideEql baz bar = no (λ ())
decideEql baz baz = yes eql


isElem' : (val : Val) -> (list : List) -> Dec (IsElemOf val list)
isElem' val nil = no (λ ())
isElem' val (cons x list) = case (decideEql val x)
  where
  case : Dec (Equal val x) -> Dec (IsElemOf val (cons x list))
  case (yes eql) = yes here
  case (no valNotX) = mapDec there (notThere valNotX) (isElem' val list)
