{-
  This editor runs on agdapad.quasicoherent.io. Your Agda code is stored on
  this server and should be available when you revisit the same Agdapad session.
  However, absolutely no guarantees are made. You should make backups by
  downloading (see the clipboard icon in the lower right corner).

  C-c C-l          check file
  C-c C-SPC        check hole
  C-c C-,          display goal and context
  C-c C-c          split cases
  C-c C-r          fill in boilerplate from goal
  C-c C-d          display type of expression
  C-c C-v          evaluate expression (normally this is C-c C-n)
  C-c C-a          try to find proof automatically
  C-z              enable Vi keybindings
  C-x C-+          increase font size
  \bN \alpha \to   math symbols
-}

data Unit : Set where
  unit : Unit

data Void : Set where

Not : Set -> Set
Not T = T -> Void

data Either (A B : Set) : Set where
  left : A -> Either A B
  right : B -> Either A B

data Pair (A B : Set) : Set where
  pair : A -> B -> Pair A B


cases : {A B C : Set} -> Either A B -> (A -> C) -> (B -> C) -> C
cases (left a) ab cb = ab a
cases (right b) ab cb = cb b

--If A then (B implies A).
ex1 : {A B : Set} -> A -> B -> A
ex1 a b = a

--If (A and true) then (A or false).
ex2 : {A : Set} -> Pair A Unit -> Either A Void
ex2 (pair a u) = left a

--If A implies (B implies C), then (A and B) implies C.
ex3 : {A B C : Set} -> (A -> B -> C) -> Pair A B -> C
ex3 fn (pair a b) = fn a b

--If A and (B or C), then either (A and B) or (A and C).
ex4 : {A B C : Set} -> Pair A (Either B C) -> Either (Pair A B) (Pair A C)
ex4 (pair a (left b)) = left (pair a b)
ex4 (pair a (right c)) = right (pair a c)

--If A implies C and B implies D, then (A and B) implies (C and D).
ex5 : {A B C D : Set} -> Pair (A -> C) (B -> D) -> Pair A B -> Pair C D
ex5 (pair ac bd) (pair a b) = pair (ac a) (bd b)

-- the law of the excluded middle (“either P or not P”)
law : {P : Set} -> Not (Not (Either P (Not P)))
law {P} x = x (right notP)
  where
  notP : Not P
  notP y = x (left y)

