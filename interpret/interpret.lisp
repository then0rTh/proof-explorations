(defpackage "INTERPRET"
            (:use "COMMON-LISP")
            (:export "INTERPRET"))
(in-package interpret)

(load "interpret-core.lisp")
(load "interpret-env.lisp")


(defun root-env ()
    (env:environment
        (list (cons '+ #'+)
              (cons '* #'*)
              (cons '- #'-)
              (cons '/ #'/)
              (cons '< #'<)
              (cons '> #'>)
              (cons '<= #'<=)
              (cons '>= #'>=)
              (cons '= #'=)
              (cons 'print #'print)
              (cons 'nil 'nil)
              (cons 't 't))
        nil))

(defun with-stdlib (program)
    `(:let ((1+ (:fn (x) (+ x 1)))
            (1- (:fn (x) (- x 1))))
         ,program))


(defun printdetail (x)  ; for debugging
    (let ((*print-level* 16))
        (print x))
    x)


; MAIN ENTRYPOINT
(defun interpret (program)
    (interpret-core:interpret (expand (with-stdlib program))
                              (root-env)))


(defun expand (code)
    (cond ((consp code)
            (expand-list (car code) (cdr code)))
          ((symbolp code)
            code)
          (T
            code)))

(defun expand-list (name args)
    (case name
        (:let
            (rewrite-let (car args) (cadr args)))
        (:lambda
            (rewrite-lambda (car args) (cadr args)))
        (otherwise
            (cons (expand name) (mapcar #'expand args)))))


; special operators

(defun rewrite-let (bindings body)
    (let ((params (mapcar #'first bindings))
          (args (mapcar #'second bindings)))  ; TODO: compose second>>expand
        `((:fn ,params ,(expand body))
          ,@(mapcar #'expand args))))


(defun rewrite-lambda (params body)
    (let ((wrapper-symbol (gensym "wrapper"))
          (user-fun-symbol (gensym "user-fun")))
      (expand
        `(:let ((user-fun (:fn ,(cons :this params)
                               ,body))
                (wrapper (:fn (,wrapper-symbol ,user-fun-symbol)
                              (:fn ,params
                                   (,user-fun-symbol (,wrapper-symbol ,wrapper-symbol ,user-fun-symbol)
                                                  ,@params)))))
             (wrapper wrapper user-fun)))))


