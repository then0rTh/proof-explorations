(defpackage "VALUE"
            (:use "COMMON-LISP")
            (:export
"VALUE" "VALUE-P" "ENSURE"
"PAIR-P" "PAIR" "PAIR-CAR" "PAIR-CDR"
"CONS_" "CONS-P" "CONS-CAR" "CONS-CDR"
"SYMBOL-TYPE" "SYMBOL-TYPE-P"
"QUOTE_" "QUOTE-P" "QUOTE-SYMBOL"
"ABSTRACTION-P" "ABSTRACTION" "ABSTRACTION-PARAM" "ABSTRACTION-BODY" "ABSTRACTION-ENV"
"PI-TYPE-P" "PI-TYPE" "PI-TYPE-DOMAIN" "PI-TYPE-CO-DOMAIN"
"U-TYPE-P" "U-TYPE"
"APPLICATION-P" "APPLICATION" "APPLICATION-OPERATOR" "APPLICATION-ARGUMENT"
"VGEN" "VGEN-P"
))

(in-package value)


; parent class

(defclass value ()
    ())

(defun value-p (val)
    (typep val 'value))

(defun ensure (val msg)
    (utils:ensure (value-p val)
                  (list msg 'not-a-value val)))


; macros for generating value structures

(defun make-getter (classname)
    (let ((sym (gensym)))
        (lambda (paramname)
            `(defmethod ,paramname (,sym)
                 (slot-value ,sym (quote ,paramname))))))

;TODO: validate params
(defun make-setter (instance-sym)
    (lambda (paramname)
        `(setf (slot-value ,instance-sym (quote ,paramname))
               ,paramname)))

(defmacro data-struct (name predicate &rest param-names)
    (let ((instance-sym (gensym))
          (param-sym (gensym)))
        `(progn
            (defclass ,name (value)
                ,param-names)
            (defun ,predicate (value)
                (typep value (quote ,name)))
            (defun ,name ,param-names
                (let ((,instance-sym (make-instance (quote ,name))))
                    ,@(mapcar (make-setter instance-sym) param-names)
                    ,instance-sym))
            ,@(mapcar (make-getter name) param-names))))


; FVAL
(data-struct abstraction abstraction-p abstraction-param abstraction-body abstraction-env)

; TVAL
(data-struct pi-type pi-type-p pi-type-domain pi-type-co-domain)

; UNIVERSE TYPE
(data-struct u-type u-type-p)

; PAIR TYPE
(data-struct pair pair-p pair-car pair-cdr)
(data-struct cons_ cons-p cons-car cons-cdr)

; SYMBOLS
(data-struct symbol-type symbol-type-p)
(data-struct quote_ quote-p quote-symbol)

; APPVAL
(data-struct application application-p application-operator application-argument)

; VGEN
(data-struct vgen vgen-p)  ; class instances have unique identity

