(defpackage "ENVIRONMENT"
            (:use "COMMON-LISP")
            (:export "UPDATE" "LOOKUP" "TRY-LOOKUP" "EMPTY"))
(in-package environment)


; ENV-PAIR represents a single variable with value

(defun env-pair-make (id val)
    (cons id val))

(defun env-pair-matches-p (env id)
    (eql (car env) id))

(defun env-pair-val (env)
    (cdr env))


; environment

(defun empty ()
    nil)

(defun update (env id val)
    (cons (env-pair-make id val)
          env))

(defun lookup (id env)
    (let ((found (try-lookup id env)))
        (when (null found)
            (print (list 'lookup id))
            (throw 'lookup id))
        found))

(defun try-lookup (id env)
    (cond ((null env)
            nil)
          ((env-pair-matches-p (car env) id)
            (env-pair-val (car env)))
          (T
            (try-lookup id (cdr env)))))
