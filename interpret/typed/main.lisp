(defpackage "TYPED"
            (:use "COMMON-LISP")
            (:export "TYPECHECK" "DECLARATIONS" "EVALUATE"))
(in-package typed)

(load "utils.lisp")
(load "core.lisp")
(load "environment.lisp")
(load "expression.lisp")
(load "syntax-sugar.lisp")


(defun process-declarations (list val-env type-env)
    (if (null list)
        (list val-env type-env)
      (let* ((decl (car list))
             (name (syntax-sugar:declaration-name decl))
             (value (syntax-sugar:declaration-value decl))
             (type (syntax-sugar:declaration-type decl)))
          (utils:ensure (null (environment:try-lookup name type-env))
                        (list 'duplicate-declaration-name name))
          (utils:ensure (typecheck val-env type-env value type)
                        (list 'wrong-declaration-type decl value type))
          (process-declarations (cdr list)
                                (environment:update val-env
                                                    name
                                                    (typed-core:evalexp val-env value))
                                (environment:update type-env
                                                    name
                                                    (typed-core:evalexp val-env type))))))

(defun declarations (declarations)
    (process-declarations declarations
                          (environment:empty)
                          (environment:empty)))


(defun typecheck (val-env type-env exp exp-type)
    (let ((exp (syntax-sugar:expand exp))
          (exp-type (syntax-sugar:expand exp-type)))
        ;(print 1)
        ;(print (typed-core:checktype val-env type-env exp-type))
        ;(print exp-type)
        ;(print 2)
        ;(print (typed-core:checkexp val-env type-env exp (typed-core:evalexp val-env exp-type)))
        (and (typed-core:checktype val-env type-env exp-type)
             (typed-core:checkexp val-env type-env exp (typed-core:evalexp val-env exp-type)))))

(defun evaluate (val-env type-env exp)
    (let* ((exp (syntax-sugar:expand exp))
           (type (typed-core:inferexp val-env type-env exp)))
        ;(print (list 1 exp type))
        (utils:ensure (typed-core:checkexp val-env type-env exp type)
                      'iferred-type-doesnt-match)
        ;(print (list 2 exp))
        (list (typed-core:evalexp val-env exp)
              type)))

