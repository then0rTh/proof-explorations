(defpackage "EXPRESSION"
            (:use "COMMON-LISP")
            (:export "U-TYPE-P" "ABSTRACTION-MAKE" "ABSTRACTION-P" "ABSTRACTION-PARAM" "ABSTRACTION-BODY" "PI-MAKE" "PI-P" "PI-PARAM" "PI-DOMAIN" "PI-BODY" "APPLICATION-MAKE" "APPLICATION-P" "APPLICATION-OP" "APPLICATION-ARGUMENT" "PAIR-P" "PAIR-CAR" "PAIR-CDR" "CONS-P" "CONS-CAR" "CONS-CDR" "CAR-P" "CAR-PARAM" "QUOTE-P" "QUOTE-SYMBOL" "SYMBOL-TYPE-P"))
(in-package expression)

(load "utils.lisp")


; this may be useless, application-p is too permissive
;(defun assert (exp)
;    (utils:ensure (or (abstraction-p exp)
;                      (pi-p exp)
;                      (application-p exp)
;                      (pair-p exp))
;                  (list 'not-an-expression exp)))

; UNIVERSE TYPE

(defun u-type-p (exp)
    (eql exp :type))


; EXP-FN

(defun abstraction-make (param body)
    (utils:ensure (symbolp param)
            "function must have only 1 argument (:abs param body) - not in list")
    (list :abs param body))

(defun abstraction-p (exp)
    (and (consp exp)
         (eql (first exp)
              :abs)
         (utils:ensure (= 3 (length exp))
                 "function must have length 3: (:abs param body)")))

(defun abstraction-param (exp &key ignore-type)
    (unless ignore-type
        (utils:ensure (symbolp (second exp))
                "function must have only 1 argument (:abs param body) - not in list"))
    (second exp))

(defun abstraction-body (exp)
    (third exp))


; EXP-TYPE

(defun pi-make (param domain body)
    (list :pi (list param domain) body))

(defun pi-p (exp)
    (and (consp exp)
         (eql (car exp)
              :pi)
         (utils:ensure (= 3 (length exp))
                 "type must have length 3 (:pi (param domain) body)")))

(defun pi-param (exp)
    (utils:ensure (= 2 (length (second exp)))
            "type must have 1 parameter and 1 domain (:pi (param domain) body)")
    (first (second exp)))

(defun pi-domain (exp)
    (utils:ensure (= 2 (length (second exp)))
            "type must have 1 parameter and 1 domain (:pi (param domain) body)")
    (second (second exp)))

(defun pi-body (exp)
    (third exp))


; EXP-TYPE-PAIR

(defun pair-p (exp)
    (and (consp exp)
         (eql (car exp)
              :pair)
         (utils:ensure (= 3 (length exp))
                       "pair must have length 3 (:pair A B)")))

(defun pair-car (exp)
    (second exp))

(defun pair-cdr (exp)
    (third exp))

(defun cons-p (exp)
    (and (consp exp)
         (eql (car exp)
              :cons)
         (utils:ensure (= 3 (length exp))
                       "cons must have length 3 (:cons a b)")))

(defun cons-car (exp)
    (second exp))

(defun cons-cdr (exp)
    (third exp))

(defun car-p (exp)
    (and (consp exp)
         (eql (car exp)
              :car)
         (utils:ensure (= 2 (length exp))
                       "car must have length 2 (:car p)")))

(defun car-param (exp)
    (second exp))


; SYMBOLS

(defun symbol-type-p (exp)
    (eql exp :symbol))

(defun quote-p (exp)
    (and (consp exp)
         (eql (car exp)
              :quote)
         (utils:ensure (= 2 (length exp))
                       "quote must have length 2 (:quote s)")
         (utils:ensure (symbolp (second exp))
                       "quote must be used on symbol (:quote s)")))

(defun quote-symbol (exp)
    (second exp))


; EXP-APP

(defun application-make (op arg)
    (list op arg))

(setf reserved-operators
      (list :abs :pi :pair :car :cdr :cons :quote))
(defun application-p (exp)
    (and (consp exp)
         (>= (length exp) 2)
         (not (find (car exp)
                    reserved-operators))))

(defun application-op (exp)
    (first exp))

(defun application-argument (exp)
    (second exp))

