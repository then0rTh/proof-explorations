(defpackage "UTILS"
            (:use "COMMON-LISP")
            (:export "ENSURE"))
(in-package utils)


(defun ensure (bool error-msg)
    (when (not bool)
        (print error-msg)
        (throw 'ensure error-msg))
    t)

