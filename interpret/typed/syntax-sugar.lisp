(defpackage "SYNTAX-SUGAR"
            (:use "COMMON-LISP")
            (:export "EXPAND" "DECLARATION-NAME" "DECLARATION-TYPE" "DECLARATION-VALUE"))
(in-package syntax-sugar)

(load "expression.lisp")


; param with type

(defun typedparam-p (exp)
    (and (consp exp)
         (= (length exp) 2)
         (symbolp (first exp))))

(defun typedparam-name (exp)
    (first exp))

(defun typedparam-type (exp)
    (second exp))


; declarations

(defun declaration-name(decl)
    (first decl))

(defun extract-type (params return)
    (if (null params)
        return
      (expression:pi-make (typedparam-name (car params))
                          (typedparam-type (car params))
                          (extract-type (cdr params) return))))

(defun declaration-type(decl)
    (let ((params (second decl))
          (return (third decl)))
        (syntax-sugar:expand (extract-type params return))))

(defun extract-value (params body)
    (if (null params)
        body
      (expression:abstraction-make (typedparam-name (car params))
                                   (extract-value (cdr params) body))))
(defun declaration-value(decl)
    (let ((params (second decl))
          (body (fourth decl)))
        (syntax-sugar:expand (extract-value params body))))


; expansions

;(-> A B) => (:pi (_ A) B)
(defun arrow-p (exp)
    (and (consp exp)
         (eql (car exp)
              :->)))

(defun arrow-types (exp)
    (cdr exp))

(defun expand-arrow (types)
    (utils:ensure types 'empty-arrow)
    (if (null (cdr types))
        (expand (car types))
      (expression:pi-make (gensym)
                          (expand (car types))
                          (expand-arrow (cdr types)))))


(defun expand-abstraction (param body)
    (cond ((null param)
            (expand body))
          ((symbolp param)
            (expression:abstraction-make param (expand body)))
          ((consp param)
            (expression:abstraction-make (car param)
                                         (expand-abstraction (cdr param)
                                                             body)))))

(defun expand-pi (params body)
    (cond ((null params)
            (expand body))
          ((typedparam-p params)
            (expression:pi-make (typedparam-name params)
                                (expand (typedparam-type params))
                                (expand body)))
          ((typedparam-p (car params))
            (expression:pi-make (typedparam-name (car params))
                                (expand (typedparam-type (car params)))
                                (expand-pi (cdr params)
                                           body)))
          (T
            (throw 'expand-pi))))

(defun expand-application (operator arguments)
    (if (null arguments)
        operator
      (expand-application (expression:application-make operator
                                                       (car arguments))
                          (cdr arguments))))

(defun expand (exp)
    (cond ((expression:abstraction-p exp)
            (expand-abstraction (expression:abstraction-param exp :ignore-type t)
                                (expression:abstraction-body exp)))
          ((expression:pi-p exp)
            (expand-pi (second exp) (third exp)))
          ((arrow-p exp)
            (expand-arrow (arrow-types exp)))
          ; application must be last (it has ambiguous syntax
          ((expression:application-p exp)
            (expand-application (expand (car exp))
                                (mapcar #'expand (cdr exp))))
          (T
            exp)))

