(defpackage "TYPED-CORE"
            (:use "COMMON-LISP")
            (:export "CHECKTYPE" "CHECKEXP" "EVALEXP" "INFEREXP"))
(in-package typed-core)

(load "environment.lisp")
(load "utils.lisp")
(load "expression.lisp")
(load "value.lisp")

;TODO: generic functions, eql specializer
;TODO: daleka-budoucnost: lambda calc: pouze app,abs(alpha-konverze)
    ; "konstanty" pair, pi
;TODO: dependent pair (bude potreba checkexp)

;SEE: Isabella vs Coq (Vec?)

;TODO: 
; type-construct (:Either T1 T2)
; constructor (:left x) (:right x) -> nejde infer, nevim druhej typ
; elim (:Either T1 T2) 2fce T1->A T2->B

; the whnf algorithm

(defun fapp (val-abstraction arg)
    (evalexp (environment:update (value:abstraction-env val-abstraction)
                                 (value:abstraction-param val-abstraction)
                                 arg)
             (value:abstraction-body val-abstraction)))


(defun app (op arg)
    ;(let ((*print-level* 3))
    ;    (print (list 'app op arg)))
    (if (value:abstraction-p op)
        (fapp op arg)
      (value:application op arg)))


(defun evalexp (env exp)
    (utils:ensure (not (value:value-p exp))
                  (list 'evalexp 'value-passed-in-as-expression exp))
    ;(print (list 'evalexp exp))
    (cond ((expression:u-type-p exp)
            (value:u-type))
          ((expression:abstraction-p exp)
            ;(print (list 'evalexp exp 'abs-make-branch))
            (value:abstraction (expression:abstraction-param exp)
                               (expression:abstraction-body exp)
                               env))
          ((expression:pi-p exp)
            (value:pi-type (evalexp env (expression:pi-domain exp))
                           (value:abstraction (expression:pi-param exp)
                                              (expression:pi-body exp)
                                              env)))
          ((expression:car-p exp)
            (value:cons-car (evalexp env (expression:car-param exp))))
          ((expression:cons-p exp)
            (value:cons_ (evalexp env (expression:cons-car exp))
                         (evalexp env (expression:cons-cdr exp))))
          ((expression:pair-p exp)
            (value:pair (evalexp env (expression:pair-car exp))
                        (evalexp env (expression:pair-cdr exp))))
          ((expression:quote-p exp)
            (value:quote_ (expression:quote-symbol exp)))
          ((expression:symbol-type-p exp)
            (value:symbol-type))
          ;application and native symbol should be last
          ((expression:application-p exp)
            ;(print (list 'evalexp exp 'app-branch))
            (app (evalexp env (expression:application-op exp))
                 (evalexp env (expression:application-argument exp))))
          ((symbolp exp)
            ;(print (list 'evalexp-lookup exp))
            (environment:lookup exp env))
          (T
            (print exp)
            (throw 'evalexp exp))))


; the conversion algorithm

(defun eqval (val1 val2)
    (value:ensure val1 'eqval)
    (value:ensure val2 'eqval)
    (cond ((and (value:application-p val1)
                (value:application-p val2))
            (and (eqval (value:application-operator val1)
                        (value:application-operator val2))
                 (eqval (value:application-argument val1)
                        (value:application-argument val2))))
          ((and (value:abstraction-p val1)
                (value:abstraction-p val2))
            (let ((canary (value:vgen)))
                (eqval (fapp val1 canary)
                       (fapp val2 canary))))
          ((and (value:pi-type-p val1)
                (value:pi-type-p val2))
            (let ((canary (value:vgen)))
                (and (eqval (value:pi-type-domain val1)
                            (value:pi-type-domain val2))
                     (eqval (fapp (value:pi-type-co-domain val1)
                                  canary)
                            (fapp (value:pi-type-co-domain val2)
                                  canary)))))
          ((and (value:u-type-p val1)
                (value:u-type-p val2))
            T)
          ((and (value:pair-p val1)
                (value:pair-p val2))
            (and (eqval (value:pair-car val1)
                        (value:pair-car val2))
                 (eqval (value:pair-cdr val1)
                        (value:pair-cdr val2))))
          ;TODO: cons?
          ((and (value:quote-p val1)
                (value:quote-p val2))
            (eql (value:quote-symbol val1)
                 (value:quote-symbol val2)))
          ((and (value:symbol-type-p val1)
                (value:symbol-type-p val2))
            T)
          (T
            (eql val1 val2))))


; type-checking and type-inference

(defun checktype (val-env type-env exp)
    (checkexp val-env type-env exp (value:u-type)))


(defun checkexp (val-env type-env exp val)
    (value:ensure val 'checkexp)
    ;(print 'checkexp)
    ;(print exp)
    ;(print val)
    (cond ((and (expression:abstraction-p exp)
                (value:pi-type-p val))
            ;(print (list 'abs-p 'pi-p))
            (let ((canary (value:vgen)))
                (checkexp (environment:update val-env
                                              (expression:abstraction-param exp)
                                              canary)
                          (environment:update type-env
                                              (expression:abstraction-param exp)
                                              (value:pi-type-domain val))
                          (expression:abstraction-body exp)
                          (fapp (value:pi-type-co-domain val)
                                canary))))
          ;TODO: infer
          ((and (expression:pi-p exp)
                (value:u-type-p val))
            ;(print (list 'pi-p 'u-p))
            (and (checktype val-env type-env (expression:pi-domain exp))
                 (checktype (environment:update val-env
                                                (expression:pi-param exp)
                                                (value:vgen))
                            (environment:update type-env
                                                (expression:pi-param exp)
                                                (evalexp val-env
                                                         (expression:pi-domain exp)))
                            (expression:pi-body exp))))
          ;TODO: cons?
          ;TODO: is infer enough?
          ;((and (expression:pair-p exp)
          ;      (value:u-type-p val))
          ;  (and (checktype val-env type-env (expression:pair-car exp))
          ;       (checktype val-env type-env (expression:pair-cdr exp))))
          (T
            ;(print (list 't-infer exp val))
            (eqval (inferexp val-env type-env exp)
                   val))))


(defun inferexp (val-env type-env exp)
    ;(print (list 'inferexp exp))
    (cond ((expression:u-type-p exp)
            (value:u-type)) ;problem?
          ;((expression:pi-p exp)
          ;  ;TODO: validate params (checktype domain & co-domain)
          ;  (value:u-type)) ;TODO? this is not in the original inferexp fn
          ((expression:cons-p exp)
            (value:pair (inferexp val-env type-env (expression:cons-car exp))
                        (inferexp val-env type-env (expression:cons-cdr exp))))
          ((expression:pair-p exp)
            ;TODO: ensure car and cdr are types (checktype)
            (value:u-type))
          ((expression:car-p exp)
            ; pair-car only works for pairs
            (value:pair-car (inferexp val-env type-env (expression:car-param exp))))
          ((expression:quote-p exp)
            ;TODO: ensure quote param is native symbol?
            (value:symbol-type))
          ((expression:symbol-type-p exp)
            (value:u-type))
          ; neumi infer abstrakce
          ; application and native symbol should be last (syntax is ambiguous)
          ((expression:application-p exp)
            (let ((inf-type (inferexp val-env
                                      type-env
                                      (expression:application-op exp))))
                (cond ((not (value:pi-type-p inf-type))
                        (print exp)
                        (throw 'inferexp-application-not-type exp))
                      ((not (checkexp val-env
                                      type-env
                                      (expression:application-argument exp)
                                      (value:pi-type-domain inf-type)))
                        (print exp)
                        (print (value:pi-type-domain inf-type))
                        (print (inferexp val-env type-env (expression:application-argument exp)))
                        (throw 'inferexp-application-mismatch exp))
                      (T
                        (fapp (value:pi-type-co-domain inf-type)
                              (evalexp val-env (expression:application-argument exp)))))))
          ((symbolp exp)
            ;(print (list 'inferexp-lookup exp))
            (environment:lookup exp type-env))
          (T
            (print (list val-env type-env exp))
            (throw 'inferexp-cannot-infer exp))))

