(load "main.lisp")

; TODO: umoznit v prostredi env vyhodnocovat dotazy (eval - normalni forma)

(setf env (typed:declarations '(

    (id ((T :type) (x T))  T
        x)

    (fst ((A :type) (x A) (y A))  A
        (id A x))

    (>> ((A :type) (B :type) (C :type) (f (:-> A B)) (g (:-> B C)))  (:-> A C)
        (:abs x
          (g ((id (:-> A B) f) x))))

    (id2 ((T :type) (x T))  T
         ((>> T T T (id T) (id T)) x))

    ;(swap ((A :type) (B :type) (p (:pair A B)))  (:pair B A)
    ;     (cons (cdr p) (car p)))
        ;(cons B A (cdr p) (car p))

)))

(defun check (code code-type)
    (print (typed:typecheck (first env)
                            (second env)
                            code
                            code-type)))

(defun check-fail (code code-type)
    (print (not (typed:typecheck (first env)
                                 (second env)
                                 code
                                 code-type))))

(defun evaluate (code)
    (let ((result (typed:evaluate (first env)
                                  (second env)
                                  code)))
        (print result)))

; TESTS

(evaluate '(id (:pair :symbol :symbol)
               (:cons (:quote foo)
                      (:quote bar))))
(evaluate '(id :symbol
               (:car (:cons (:quote foo)
                            (:quote bar)))))
;(evaluate '(id2 (:pi (T :type) (:-> T T))))

;fst
(check '(:abs (T x y)
          x)
       '(:pi ((T :type) (x T) (y T))
          T))

;fst with unnecesary type param
(check '(:abs (T K x y)
          x)
       '(:pi ((T :type) (K :type))
          (:-> T T
            T)))
(check '(:abs T
          (:abs K
            (:abs x
              (:abs y
                x))))
       '(:pi (T :type)
          (:pi (K :type)
            (:pi (x T)
              (:pi (y T)
                T)))))

;bad fst with unnecesary type param
(check-fail '(:abs (T K x y)
               x)
            '(:pi ((T :type) (K :type))
               (:-> T T
                 K)))
(check-fail '(:abs T
               (:abs K
                 (:abs x
                   (:abs y
                     x))))
            '(:pi (T :type)
               (:pi (K :type)
                 (:pi (x T)
                   (:pi (y T)
                     K)))))


; symbols

(check '(:abs x
          x)
       '(:-> :symbol
          :symbol))

(check '(:quote asdf)
       :symbol)


; pair basics

(check '(:abs (A B p)
          p)
       '(:pi ((A :type) (B :type))
          (:-> (:pair A B)
            (:pair A B))))

(check '(:abs (A B p)
          (:car p))
       '(:pi ((A :type) (B :type))
          (:-> (:pair A B)
            A)))

(check '(:abs (A B x y)
          (:cons x y))
       '(:pi ((A :type) (B :type))
          (:-> A B
            (:pair A B))))


; LOGIC

;--If A then (B implies A).
;ex1 : {A B : Set} -> A -> B -> A
;ex1 a b = a
(check '(:abs A
          (:abs B
            (:abs x
              (:abs y
                x))))
       '(:pi (A :type)
          (:pi (B :type)
            (:pi (x A)
              (:pi (y B)
                A)))))

;--If (A and true) then (A or false).
;ex2 : {A : Set} -> Pair A Unit -> Either A Void
;ex2 (pair a u) = left a

;--If A implies (B implies C), then (A and B) implies C.
;ex3 : {A B C : Set} -> (A -> B -> C) -> Pair A B -> C
;ex3 fn (pair a b) = fn a b

;--If A and (B or C), then either (A and B) or (A and C).
;ex4 : {A B C : Set} -> Pair A (Either B C) -> Either (Pair A B) (Pair A C)
;ex4 (pair a (left b)) = left (pair a b)
;ex4 (pair a (right c)) = right (pair a c)

;--If A implies C and B implies D, then (A and B) implies (C and D).
;ex5 : {A B C D : Set} -> Pair (A -> C) (B -> D) -> Pair A B -> Pair C D
;ex5 (pair ac bd) (pair a b) = pair (ac a) (bd b)

;-- the law of the excluded middle (“either P or not P”)
;law : {P : Set} -> Not (Not (Either P (Not P)))
;law {P} x = x (right notP)
;  where
;  notP : Not P
;  notP y = x (left y)


