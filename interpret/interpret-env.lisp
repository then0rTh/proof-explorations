(defpackage "ENV"
            (:use "COMMON-LISP")
            (:export "ENVIRONMENT" "ENVIRONMENT-P" "GET-VALUE"))
(in-package env)

(load "interpret-utils.lisp")



(defun environment (symbol-value-pairs parent-env)
    "Constructor for intnernal representation of environment"
    ;(print "ENV")
    ;(print pairs)
    (list 'environment symbol-value-pairs parent-env))

(defun environment-p (env)
    (eql (first env) 'environment))

(defun environment-pairs (env)
    (second env))

(defun environment-parent-env (env)
    (third env))


(defun get-value (env name)
    (cond ((null env)
            (print "ERROR: undefined variable")
            (princ name)
            (throw 'interpret-error-undefined-variable name))
          ((environment-p env)
            (get-value-iter (environment-pairs env)
                            name
                            (environment-parent-env env)))))

(defun get-value-iter (pairs name parent-env)
    (cond ((null pairs)
            (get-value parent-env name))
          ((interpret-utils:sym-equal-global (caar pairs) name)
            (cdar pairs))
          (T
            (get-value-iter (cdr pairs) name parent-env))))

