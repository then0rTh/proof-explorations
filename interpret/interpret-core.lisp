(defpackage "INTERPRET-CORE"
            (:use "COMMON-LISP")
            (:export "INTERPRET" "ENVIRONMENT"))
(in-package interpret-core)

(load "interpret-env.lisp")



; MAIN ENTRYPOINT
(defun interpret (program root-env)
    (interpret-eval program
                    root-env))

(defun interpret-eval (code env)
    ;(print "EVAL")
    ;(print code)
    (cond ((consp code)
            (eval-list (car code) (cdr code) env))
          ((symbolp code)
            (env:get-value env code))
          (T
            code)))

(defun eval-list (name args env)
    (case name
        (:fn
            (eval-specop-lambda-basic (car args) (cadr args) env))
        (:if
            (eval-specop-if (car args) (cadr args) (caddr args) env))
        (otherwise
            (eval-function name args env))))


; special operators

(defun eval-specop-lambda-basic (params body env)
    (user-function params body env))


(defun eval-specop-if (condition positive-branch negative-branch env)
    (interpret-eval (if (interpret-eval condition env)
                        positive-branch
                        negative-branch)
                    env))


; fun

(defun eval-function (name args env)
    (let ((fun (interpret-eval name env))
          (ar (mapcar (lambda (a) (interpret-eval a env))
                      args)))
        (cond ((functionp fun)
                (apply fun ar))
              ((user-function-p fun)
                (user-function-apply fun ar))
              (t
                (print "ERROR: not a function")
                (princ name)
                (print args)
                (print ar)
                (print env)
                (throw 'interpret-error-not-a-function name)))))


(defun user-function (params body env)
    "Constructor for internal representation of user-function"
    (list 'user-function params body env))

(defun user-function-p (fun)
    (eql (car fun) 'user-function))

(defun user-function-params (fun)
    (second fun))

(defun user-function-body (fun)
    (third fun))

(defun user-function-env (fun)
    (fourth fun))


(defun user-function-apply (fun arg-values)
    (let ((params (user-function-params fun))
          (body (user-function-body fun))
          (env (user-function-env fun)))
        (when (not (= (length params)
                      (length arg-values)))
            (print "ERROR: incorrect number of arguments")
            (princ params)
            (princ arg-values)
            (throw 'interpret-error-arg-mismatch params))
        (interpret-eval body
                        (env:environment (mapcar #'cons params arg-values)
                                         env))))

