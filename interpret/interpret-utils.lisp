(defpackage "INTERPRET-UTILS"
            (:use "COMMON-LISP")
            (:export "SYM-EQUAL-GLOBAL" "SYM-GLOBAL-CASE"))
(in-package interpret-utils)



(defun sym-equal-global (sym1 sym2)
    "Compare case-insensitive equality of symbols, regardless of package"
    (string-equal (symbol-name sym1)
                  (symbol-name sym2)))

