(load "interpret.lisp")

(setf *print-level* 3)

(defun test (name expected-result code)
    (print name)
    (princ " ... ")
    (let ((actual-result (interpret:interpret code)))
        (if (eql expected-result actual-result)
            (princ "OK")
            (format t "ERR: expected ~a but got ~a" expected-result actual-result))))


(test "if"
      7
      '((:fn () (:if nil 5 7))))

(test "addition"
      (+ 20 (* 2 14))
      '(:let ((x (+ 1 3))
              (add (:fn (x) (+ x 10))))
          (+ (:let ((x 10))
                (add x))
             (* 2 (add x)))
      )
)

(test "fib"
      (labels ((fib (n)
                    (if (< n 2)
                        1
                      (+ (fib (- n 2)) (fib (- n 1))))))
              (fib 10))
      '(:let ((fib (:lambda (n)
                      (:if (< n 2)
                          1
                          (+ (:this (- n 2)) (:this (- n 1)))))))
           (fib 10)))

(test "fib-iter"
      (labels ((fib-iter (n-left prev current)
                    (if (<= n-left 0)
                        current
                      (fib-iter (- n-left 1) current (+ prev current))))
               (fib (n)
                    (fib-iter n 0 1)))
              (fib 10))
      '(:let ((fib-iter (:lambda (n-left prev current)
                            (:if (<= n-left 0)
                                 current
                               (:this (1- n-left) current (+ prev current))))))
           (fib-iter 10 0 1)))

(test "fib-iter-anonym"
      (labels ((fib-iter (n-left prev current)
                    (if (<= n-left 0)
                        current
                      (fib-iter (- n-left 1) current (+ prev current))))
               (fib (n)
                    (fib-iter n 0 1)))
              (fib 10))
      '((:lambda (n-left prev current)
          (:if (<= n-left 0)
               current
             (:this (1- n-left) current (+ prev current))))
        10
        0
        1))


