{-
  This editor runs on agdapad.quasicoherent.io. Your Agda code is stored on
  this server and should be available when you revisit the same Agdapad session.
  However, absolutely no guarantees are made. You should make backups by
  downloading (see the clipboard icon in the lower right corner).

  C-c C-l          check file
  C-c C-SPC        check hole
  C-c C-,          display goal and context
  C-c C-c          split cases
  C-c C-r          fill in boilerplate from goal
  C-c C-d          display type of expression
  C-c C-v          evaluate expression (normally this is C-c C-n)
  C-c C-a          try to find proof automatically
  C-z              enable Vi keybindings
  C-x C-+          increase font size
  \bN \alpha \to   math symbols
-}

data DoorState : Set where
  opened : DoorState
  closed : DoorState
  locked : DoorState


data IsKnockable : DoorState -> Set where
  closedIsKnockable : IsKnockable closed
  lockedIsKnockable : IsKnockable locked

data DoorAction : DoorState -> DoorState -> Set where
  doOpen : DoorAction closed opened
  doClose : DoorAction opened closed
  doLock : DoorAction closed locked
  doUnlock : DoorAction locked closed
  wait : {state : DoorState} -> DoorAction state state
  -- cannot knock on open doors
  knock : {state : DoorState} -> {proof : IsKnockable state} -> DoorAction state state


data Sequence : DoorState -> DoorState -> Set where
  last : {a b : DoorState} -> DoorAction a b -> Sequence a b
  then : {a b c : DoorState} -> DoorAction a b -> Sequence b c -> Sequence a c


example : Sequence locked locked
example =
  let
    x = lockedIsKnockable
  in
  (then (knock {proof = x})  -- why can't it guess the correct implicit arg?
  (then wait
  (then doUnlock
  (then (knock {proof = closedIsKnockable})
  (then wait
  (then doOpen
  (then wait
  (then doClose
  (last doLock
  )))))))))

