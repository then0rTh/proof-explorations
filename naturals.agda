
{-
  This editor runs on agdapad.quasicoherent.io. Your Agda code is stored on
  this server and should be available when you revisit the same Agdapad session.
  However, absolutely no guarantees are made. You should make backups by
  downloading (see the clipboard icon in the lower right corner).

  C-c C-l          check file
  C-c C-SPC        check hole
  C-c C-,          display goal and context
  C-c C-c          split cases
  C-c C-r          fill in boilerplate from goal
  C-c C-d          display type of expression
  C-c C-v          evaluate expression (normally this is C-c C-n)
  C-c C-a          try to find proof automatically
  C-z              enable Vi keybindings
  C-x C-+          increase font size
  \bN \alpha \to   math symbols

  "C-c" means "<Ctrl key> + c". In case your browser is intercepting C-c,
  you can also use C-u. For pasting code into the Agdapad, see the clipboard
  icon in the lower right corner.

  In text mode, use <F10> to access the menu bar, not the mouse.
-}

-- a
data Nat : Set where
  zero : Nat
  succ : Nat → Nat


-- addition
_+_ : Nat -> Nat -> Nat
_+_ zero x = x
_+_ (succ x) y = succ (x + y)


-- multiplication
_*_ : Nat -> Nat -> Nat
_*_ zero b = zero
_*_ (succ a) b = b + (a * b)


-- a = b
data Equal {T : Set} : T -> T -> Set where
  refl : {x : T} -> Equal x x


-- Equal utils

eqlSucc : {x y : Nat} -> Equal x y -> Equal (succ x) (succ y)
eqlSucc refl = refl

eqlPred : {x y : Nat} -> Equal (succ x) (succ y) -> Equal x y
eqlPred refl = refl

eqlMapBoth : {x x' y y' : Nat} -> Equal x x' -> Equal y y' -> Equal x y -> Equal x' y'
eqlMapBoth refl refl refl = refl


-- prove Equal is an equality

eqlReflexive : {x : Nat} -> Equal x x
eqlReflexive = refl

eqlSymmetry : {T : Set} -> {x y : T} -> Equal x y -> Equal y x
eqlSymmetry refl = refl

eqlTransitive : {T : Set} -> {x y z : T} -> Equal x y -> Equal y z -> Equal x z
eqlTransitive refl refl = refl

infixl 3 _<=>_
_<=>_ = eqlTransitive


--

infix 3 begin_
begin_ : {T : Set} -> (x : T) -> T
begin_ x = x

infixr 2 _=<_>_
_=<_>_ : {T : Set} -> (x : T) -> {y z : T} -> Equal x y -> Equal y z -> Equal x z 
_=<_>_ {T} x p1 p2 = eqlTransitive p1 p2

infixr 2 _=<>_
_=<>_ : {T : Set} -> (x : T) -> {y : T} -> Equal x y -> Equal x y
_=<>_ {T} x {y} p1  = p1

infix 3 _end
_end : {T : Set} -> (x : T) -> Equal x x
_end x = refl

data Bool : Set where
  true false : Bool
not : Bool -> Bool
not true = false
not false = true

doubleNotEqual' : (b : Bool) -> Equal (not (not b)) b
doubleNotEqual' true = refl
doubleNotEqual' false = refl

doubleNotEqual : (b : Bool) -> Equal (not (not b)) b
doubleNotEqual true =
  begin
    not (not true)
  =<>
    not false
  =<>
    true
  end    
doubleNotEqual false =
  begin
    not (not false)
  =<>
    not true
  =<>
    false
  end

-- prove Equal is congruence

eqlCompatibleAdd : {x y x' y' : Nat} -> Equal x x' -> Equal y y' -> Equal (x + y) (x' + y')
eqlCompatibleAdd refl refl = refl

-- TODO: minus  -- probably not possible on Nat

eqlCompatibleMul : {x y x' y' : Nat} -> Equal x x' -> Equal y y' -> Equal (x * y) (x' * y')
eqlCompatibleMul refl refl = refl

-- TODO: power


-- iterative addition

addIter : Nat -> Nat -> Nat
addIter zero b = b
addIter (succ a) b = addIter a (succ b)

addIterIsSame : (x y : Nat) -> Equal (x + y) (addIter x y)
addIterIsSame zero y = refl
addIterIsSame (succ x) y =
  begin
    (succ x) + y
  =<>
    succ (x + y)
  =< eqlSucc (addIterIsSame x y) >
     succ (addIter x y)
  =< addIterStep x y >
     addIter x (succ y)
  =<>
    addIter (succ x) y
  end
  where
  addIterStep : (a b : Nat) -> Equal (succ (addIter a b)) (addIter a (succ b))
  addIterStep zero b = refl
  addIterStep (succ a) b = addIterStep a (succ b)


-- a + (b + c) = (a + b) + c
assoc : (a b c : Nat) -> Equal (a + (b + c)) ((a + b) + c)
assoc zero b c = refl
assoc (succ a) b c = eqlSucc (assoc a b c)


neutralLeft : {x : Nat} -> Equal (zero + x) x
neutralLeft  = refl

neutralRight : {x : Nat} -> Equal (x + zero) x
neutralRight {zero} = refl
neutralRight {succ a} = eqlSucc (neutralRight {a})

neutralBoth : {x : Nat} ->  Equal (zero + x) (x + zero)
neutralBoth = neutralLeft <=> (eqlSymmetry neutralRight)

neutralBoth' : {x : Nat} ->  Equal (zero + x) (x + zero)
neutralBoth' {x} = eqlSymmetry neutralRight

neutralBoth'' : {x : Nat} ->  Equal (zero + x)  (x + zero)
neutralBoth'' {x} =
  begin
    zero + x
  =<>
    x
  =< eqlSymmetry neutralRight >
    x + zero
  end

dist1l : {a b : Nat} -> Equal ((succ a) + b) (succ (a + b))
dist1l = refl

dist1r : {a b : Nat} -> Equal (a + (succ b)) (succ (a + b))
dist1r {zero} {b} = refl
dist1r {succ a} {b} = eqlSucc (dist1r {a} {b})

commSuccOut : (x y a b : Nat) -> Equal (succ (succ (x + y))) (succ (succ (a + b))) -> Equal ((succ x) + (succ y)) ((succ a) + (succ b))
commSuccOut x y a b eq =
  eqlMapBoth (mapRight {succ x}) (mapRight {succ a})
    (eqlSucc
      (eqlMapBoth (mapLeft {x}) (mapLeft {a})
        (eqlPred eq)))
  where
  mapLeft : {x y : Nat} -> Equal (succ (x + y)) ((succ x) + y)
  mapLeft {x} = eqlSymmetry (dist1l {x})
  mapRight : {x y : Nat} -> Equal (succ (x + y)) (x + (succ y))
  mapRight {x} = eqlSymmetry (dist1r {x})

commSucc : (x y a b : Nat) -> Equal (x + y) (a + b) -> Equal ((succ x) + (succ y)) ((succ a) + (succ b))
commSucc x y a b eq = commSuccOut x y a b (eqlSucc (eqlSucc eq))

commSucc'' : (x y a b : Nat) -> Equal (x + y) (a + b) -> Equal ((succ x) + (succ y)) ((succ a) + (succ b))
commSucc'' x y a b p =
  dist1r {succ x}
  <=>
  eqlSucc (eqlSucc p)
  <=>
  eqlSymmetry (dist1r {succ a})

commSucc' : (x y a b : Nat) -> Equal (x + y) (a + b) -> Equal ((succ x) + (succ y)) ((succ a) + (succ b))
commSucc' x y a b p =
  begin
    (succ x) + (succ y)
  =< dist1r {succ x} >
    succ ((succ x) + y)
  =<>
    succ (succ (x + y))
  =< eqlSucc (eqlSucc p) >
    succ (succ (a + b))
  =<>
    succ ((succ a) + b)
  =< eqlSymmetry (dist1r {succ a}) >
    (succ a) + (succ b)
  end

-- x + y = y + x
commutative : (x y : Nat) -> Equal (x + y) (y + x)
commutative zero zero = refl
commutative zero (succ a) = neutralBoth {succ a}
commutative (succ a) zero = eqlSymmetry (neutralBoth {succ a})
commutative (succ a) (succ b) = commSucc a b b a (commutative a b)



-- multiply proofs

two = succ (succ zero)
three = succ (succ (succ zero))
six = succ (succ (succ (succ (succ (succ zero)))))
mulTest1 : Equal (two * three) six
mulTest1 = refl


-- ax + bx = (a + b)x
mulDistributive : {a b x : Nat} -> Equal ((a * x) + (b * x)) ((a + b) * x)
mulDistributive {zero} {b} {x} = refl
mulDistributive {succ a} {b} {x} =
  begin
    (succ a * x) + (b * x)
  =<>
    (x + (a * x)) + (b * x)
  =< eqlSymmetry (assoc x (a * x) (b * x)) >
    x + ((a * x) + (b * x))
  =< eqlCompatibleAdd {x} refl (mulDistributive {a}) >
    x + ((a + b) * x)
  =<>
    succ (a + b) * x
  =<>
    (succ a + b) * x
  end


-- a * (b * c) = (a * b) * c
mulAssoc : (a b c : Nat) -> Equal (a * (b * c)) ((a * b) * c)
mulAssoc zero b c = refl
mulAssoc (succ a) b c =
  begin
    (succ a) * (b * c)
  =<>
    (b * c) + (a * (b * c))
  =< eqlCompatibleAdd {b * c} refl (mulAssoc a b c) >
    (b * c) + ((a * b) * c)
  =< mulDistributive {b} >
    (b + (a * b)) * c
  =<>
    ((succ a) * b) * c
  end


-- fib

fibNaive : Nat -> Nat
fibNaive zero = zero
fibNaive (succ zero) = succ zero
fibNaive (succ (succ a)) = (fibNaive a) + (fibNaive (succ a))


fibIterHelp : Nat -> Nat -> Nat -> Nat
fibIterHelp zero prev current = prev
fibIterHelp (succ n) prev current = fibIterHelp n current (prev + current)

fibIter : Nat -> Nat
fibIter n = fibIterHelp n zero (succ zero)


naiveDef : {n : Nat} -> Equal ((fibNaive n) + (fibNaive (succ n))) (fibNaive (succ (succ n)))
naiveDef = refl


iterDef : {n : Nat} -> Equal ((fibIter n) + (fibIter (succ n))) (fibIter (succ (succ n)))
iterDef {zero} = refl
iterDef {succ zero} = refl
iterDef {succ (succ n)} = {!!}
  where
  prev = iterDef {n}
  curr = iterDef {succ n}


fibEqlStep : {n : Nat} -> Equal (fibNaive n) (fibIter n)
                       -> Equal (fibNaive (succ n)) (fibIter (succ n))
                       -> Equal (fibNaive (succ (succ n))) (fibIter (succ (succ n)))
fibEqlStep {n} p1 p2 = eqlMapBoth (naiveDef {n}) (iterDef {n}) (eqlCompatibleAdd p1 p2)


fibEql : (n : Nat) -> Equal (fibNaive n) (fibIter n)
fibEql zero = refl
fibEql (succ zero) = refl
fibEql (succ (succ n)) = fibEqlStep {n} (fibEql n) (fibEql (succ n))
