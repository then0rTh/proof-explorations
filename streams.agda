{-
  This editor runs on agdapad.quasicoherent.io. Your Agda code is stored on
  this server and should be available when you revisit the same Agdapad session.
  However, absolutely no guarantees are made. You should make backups by
  downloading (see the clipboard icon in the lower right corner).

  C-c C-l          check file
  C-c C-SPC        check hole
  C-c C-,          display goal and context
  C-c C-c          split cases
  C-c C-r          fill in boilerplate from goal
  C-c C-d          display type of expression
  C-c C-v          evaluate expression (normally this is C-c C-n)
  C-c C-a          try to find proof automatically
  C-z              enable Vi keybindings
  C-x C-+          increase font size
  \bN \alpha \to   math symbols
-}

data Nat : Set where
  zero : Nat
  succ : Nat -> Nat

data Stream (T : Set) : Set where
  stream : T -> (T -> T) -> Stream T

step : {T : Set} -> Stream T -> Stream T
step (stream value next) = stream (next value) next

countFrom : Nat -> Stream Nat
countFrom n = stream n (\x -> succ x)

takeNth : {T : Set} -> Nat -> Stream T -> T
takeNth zero (stream value _) = value
takeNth (succ n) str = takeNth n (step str)

-------

data Unit : Set where
  unit : Unit

Lazy : Set -> Set
Lazy T = Unit -> T

force : {T : Set} -> Lazy T -> T
force lazy = lazy unit

data Stream' (T : Set) : Set where
  stream'Cons : T -> Lazy (Stream' T) -> Stream' T

-- Termination checking failed
--countFrom' : Nat -> Stream' Nat
--countFrom' n = stream'Cons n (\_ -> countFrom' (succ n))

takeNth' : {T : Set} -> Nat -> Stream' T -> T
takeNth' zero (stream'Cons value rest) = value
takeNth' (succ nth) (stream'Cons value rest) = takeNth' nth (force rest)
