(defpackage "BC-CORE"
            (:use "COMMON-LISP")
            (:export "expression"))
(in-package bc-core)

(load "environment.lisp")

;TODO: generic functions, eql specializer
;TODO: daleka-budoucnost: lambda calc: pouze app,abs(alpha-konverze)
    ; "konstanty" pair, pi
;TODO: dependent pair (bude potreba checkexp)

;SEE: Isabella vs Coq (Vec?)

;TODO: 
; type-construct (:Either T1 T2)
; constructor (:left x) (:right x) -> nejde infer, nevim druhej typ
; elim (:Either T1 T2) 2fce T1->A T2->B



; EXPRESSION data strucutre

(defclass expression ()
    ())

(defclass e-var (expression)
    ((name :type symbol
           :reader name
           :initarg :name)))

(defclass e-abstraction (expression)
    ((param :type var
            :reader param
            :initarg :param)
     ;(param-type :type expression
     ;            :reader param-type
     ;            :initarg :param-type)
     (body :type expression
           :reader body
           :initarg :body)))

(defclass e-application (expression)
    ((operator :type expression
               :reader operator
               :initarg :operator)
     (argument :type expression
               :reader argument
               :initarg :argument)))



; PARSE list -> expression
; only converts lists/symbols to our datastructure

(defmethod parse-code ((code symbol))
    (make-instance 'e-var :name code))

(defmethod parse-code ((code cons))
    (cond ((and (= 3 (length code))
                (eql :abs (car code)))
            (parse-code-abstraction code))
          ((= 2 (length code))
            (parse-code-application code))
          (t
            (error 'parse-code-invalid code))))


(defun parse-code-abstraction (code)
    (make-instance 'e-abstraction
                   :param (parse-code (second code))
                   ;:param-type (parse-code (second (second code)))
                   :body (parse-code (third code))))

(defun parse-code-application (code)
    (make-instance 'e-application
                   :operator (parse-code (first code))
                   :argument (parse-code (second code))))



; VALUE data structure

(defclass value ()
    ())

(defclass var (value)
    ((name :type symbol
           :reader name
           :initarg :name)
     (id :type symbol
         :reader id
         :initform (gensym))
     (free-p :type symbol
             :reader free-p
             :initform nil
             :initarg :free-p)))

(defclass abstraction (value)
    ((param :type var
            :reader param
            :initarg :param)
     ;(param-type :type value
     ;            :reader param-type
     ;            :initarg :param-type)
     (body :type value
           :reader body
           :initarg :body)))

(defclass application (value)
    ((operator :type value
               :reader operator
               :initarg :operator)
     (argument :type value
               :reader argument
               :initarg :argument)))

; TODO: pi?



; VALUE EQUALITY

(defmethod value-eql ((a value) (b value))
    (value-eql-normalized (normalize a)
                          (normalize b)))

(defmethod value-eql-normalized ((a var) (b var))
    (or (eql (id a) (id b))
        (and (eql (name a) (name b))
             (free-p a)
             (free-p b)
             (error "TODO: should free variables be eql?"))))

; in case we couldn't beta-reduce this application, when operator is constant (pi)
(defmethod value-eql-normalized ((a application) (b application))
    (and (value-eql (operator a)
                    (operator b))
         (value-eql (argument a)
                    (argument b))))

(defmethod value-eql-normalized ((a abstraction) (b abstraction))
    (let ((canary (make-instance 'var :name x)))
        (value-eql (make-instance 'application
                                  :operator a
                                  :argument canary)
                   (make-instance 'application
                                  :operator b
                                  :argument canary))))

(defmethod value-eql-normalized ((a value) (b value))
    nil)  ; different types of a and b



; NORMALIZATION + BETA-REDUCTION

(defmethod normalize ((x abstraction))
    ; TODO: normalize body?
    x)

(defmethod normalize ((x application))  ; TODO: noramlize result/body?
    (beta-reduce (normalize (operator x))
                 (argument x)))  ; TODO: normalize argument?

(defmethod normalize ((x var))
    ; beta-reduce should take care of this
    x)


(defmethod beta-reduce ((abs abstraction) (arg value))
    (syntactic-inline (body abs)
                      (param abs)
                      arg))


(defmethod syntactic-inline ((expr var) (param var) (param-val value))
    (if (value-eql-normalized expr param)
        param-val
      expr))

(defmethod syntactic-inline ((expr abstraction) (param var) (param-val value))
    ; if (param expr) has same name as param then we can stop? (instead of gensym)
    (make-instance 'abstraction
                   :param (param expr)
                   :body (syntactic-inline (body expr)
                                           param
                                           param-val)))

(defmethod syntactic-inline ((expr application) (param var) (param-val value))
    (make-instance 'application
                   :operator (syntactic-inline (operator expr)
                                               param
                                               param-val)
                   :argument (syntactic-inline (argument expr)
                                               param
                                               param-val)))


; TYPECHECK (value, type) -> error|void

(defmethod typecheck ((val var) (typ value))
    ; TODO: lookup val (infer?)
    ; check its same as typ
    nil)

(defmethod typecheck ((val abstraction) (typ value))
    ; TODO: destructure typ (assert pi)
    ; update type-env: val's param :: typ's param
    ; typecheck val's body againts typ's return (in update type-env)
    nil)

(defmethod typecheck ((val application) (typ value))
    nil)


; INFER (value) -> type

(defmethod infer ((val var))
    ;TODO: lookup in type-env
    nil)

(defmethod infer ((val abstraction))
    nil)

(defmethod infer ((val application))
    ; TODO: infer type of operator in type-env (assert pi)
    ;       ^ destructure pi into param-type and return-type
    ; infer type of argument in type-env
    ; if param-type is same as infered type of argument return return-type
    nil)

;unifikace
;univerza
; PI jako konstanta
; U jako konstanta
; jednodussi (bez dep) = hinley-miller (unifikace)



; EVAL expression -> value
; handles free variables and alpha-conversions thru gensym

(defmethod add-param-definition ((params environment:environment) (new-param e-var))
    (environment:update params
                        (name new-param)
                        (make-instance 'var
                                       :name (name new-param))))

(defmethod eval-expression ((expr e-var) (params environment:environment))
    (or (environment:try-lookup params (name expr))
        (make-instance 'var
                       :name (name expr)
                       :free-p t)))

(defmethod eval-expression ((expr e-abstraction) (params environment:environment))
    (let ((params (add-param-definition params
                                        (param expr))))
        (make-instance 'abstraction
                       :param (environment:lookup params
                                                  (name (param expr)))
                       ;:param-type (eval-expression (param-type expr)
                       ;                             params)
                       :body (eval-expression (body expr)
                                              params))))

(defmethod eval-expression ((expr e-application) (params environment:environment))
    (make-instance 'application
                   :operator (eval-expression (operator expr)
                                              params)
                   :argument (eval-expression (argument expr)
                                              params)))



; TESTING

(defun dbg-print (x)
    (print "")
    (print x)
    (print (list (operator x) (argument x)))
    (let ((abs (operator x)))
        (print (list :abs (param abs)(body abs)))
    )
)


(let ((x (parse-code '((:abs a a) a))))
    (dbg-print x)
    (dbg-print (eval-expression x (environment:empty)))
)

