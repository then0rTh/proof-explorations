(defpackage "ENVIRONMENT"
                (:use "COMMON-LISP")
                (:export "ENVIRONMENT" "UPDATE" "LOOKUP" "TRY-LOOKUP" "EMPTY"))
(in-package environment)


(defun empty ()
    (make-instance 'environment))


(defclass environment ()
    ((plist :type list
            :reader plist
            :initform nil
            :initarg :plist)))

(defmethod update ((old-env environment) (key symbol) value)
    (make-instance 'environment
                   :plist (plist-cons key
                                      value
                                      (plist old-env))))

(defmethod lookup ((env environment) (key symbol))
    (or (try-lookup env key)
        (error 'not-found key)))

(defmethod try-lookup ((env environment) (key symbol))
    (getf (plist env) key))


(defun plist-cons (key val list)
    (cons key (cons val list)))

