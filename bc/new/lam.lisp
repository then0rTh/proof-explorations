(load "record.lisp")
(defpackage "LAM"
            (:use "COMMON-LISP" "RECORD")
            (:export "EXPRESSION" "NORMAL" "BETA" "SAME"
                     "PARSE" "ENCODE"
                     "VARIABL"
                     "ABSTRACTION" "PARAM" "BODY"
                     "APPLICATION" "OPERATOR" "ARGUMENT"))
(in-package lam)



(defclass expression ()
    ())

(defmethod same ((a expression) (b expression))
    nil)


;;; VARIABL ;;;

(defrecord variabl (expression)
    (name-symbol symbol))

(defmethod same ((a variabl) (b variabl))
    "Uses equivalence of lisp symbols (we are using gensym - `genvar`)"
    (eql (name-symbol a)
         (name-symbol b)))

(defun genvar ()
    "Guaranteed to not be `same` as any other `variabl`"
    (make-instance 'variabl
                   :name-symbol (gensym)))


;;; ABSTRACTION ;;;

(defrecord abstraction (expression)
    (param variabl)
    (body expression))

(defmethod same ((a abstraction) (b abstraction))
    "Alpha equivalence: \x.x and \y.y are the same"
    (let ((canary (genvar)))
        (same (normal (beta a canary))
              (normal (beta b canary)))))


;;; APPLICATION ;;;

(defrecord application (expression)
    (operator expression)
    (argument expression))

(defmethod same ((a application) (b application))
    "Both `a` and `b` should already be in normal form"
    (and (same (operator a)
               (operator b))
         (same (argument a)
               (argument b))))


;;;;;;;;;;;;;;;;;;


(defmethod alpha ((abs abstraction))
    "creates new abstraction with it's param changed to gensym"
    ;(pprint (list "alpha" abs))
    (let ((new-param (genvar)))
        (make-instance 'abstraction
                       :param new-param
                       :body (beta abs new-param))))


(defmethod beta ((op abstraction) (arg expression))
    "Return body of `op`, with all occurences of param replaced for `arg`"
    ;(pprint (list "beta" op arg))
    (substition (body op) (param op) arg))


(defmethod substition ((expr variabl) (name variabl) (value expression))
    "The final leaf of substition"
    ;(pprint (list "substition var" expr name value (same expr name)))
    (if (same expr name)
        value
      expr))

(defmethod substition ((expr abstraction) (name variabl) (value expression))
    "In the body of `expr`, replace all free occurences of `name` with `value`"
    ;(pprint (list "substition abs" expr name value))
    (let ((safe-expr (alpha expr)))
        (make-instance 'abstraction
                       :param (param safe-expr)
                       :body (substition (body safe-expr) name value))))

(defmethod substition ((expr application) (name variabl) (value expression))
    "Replace all free `name` with `value` in both the operator and argument"
    ;(pprint (list "substition app" expr name value))
    (make-instance 'application
                   :operator (substition (operator expr) name value)
                   :argument (substition (argument expr) name value)))


(defmethod normal ((expr variabl))
    "Variable is already in normal form"
    ;(pprint "normal var")
    expr)

(defmethod normal ((expr abstraction))
    "Abstraction is already in normal form"
    ;(pprint "normal abs")
    expr)

(defmethod normal ((expr application))
    "Normalize operator. If possible, apply argument and normalize result"
    ;(pprint (list "normal app" expr))
    (let ((abs (normal (operator expr)))
          (arg (argument expr)))
        (if (typep abs 'abstraction)
            (normal (beta abs arg))
          (make-instance 'application
                         :operator abs
                         :argument arg))))
          ; leave as is?
          ; Or check against whitelist of external symbols? (e.g. :pi)


;;;;;;;;;;;;;;;


(defmethod parse ((code symbol))
    "turn the list syntax into `expression` instance"
    (make-instance 'variabl
                   :name-symbol code))

(defmethod parse ((code list))
    "turn the list syntax into `expression` instance"
    (unless (= 3 (length code))
            (error "parse: invalid length of code: ~S" code))
    (cond ((eql :abs (first code))
           (parse-abstraction code))
          ((eql :app (first code))
           (parse-application code))
          (t
            (error "parse: invalid code: ~S" code))))


(defun parse-abstraction (code)
    (make-instance 'abstraction
                   :param (parse (second code))
                   :body (parse (third code))))

(defun parse-application (code)
    (make-instance 'application
                   :operator (parse (second code))
                   :argument (parse (third code))))

;;;;;

(defmethod encode ((expr variabl))
    "dump an `expression` instance into list syntax"
    (name-symbol expr))

(defmethod encode ((expr abstraction))
    "dump an `expression` instance into list syntax"
    (list :abs
          (encode (param expr))
          (encode (body expr))))

(defmethod encode ((expr application))
    "dump an `expression` instance into list syntax"
    (list :app
          (encode (operator expr))
          (encode (argument expr))))


;;;;;;;;;;;;;;;;;
;;;;; TESTS ;;;;;

(defun tst-equal (a b)
    (same (parse a)
          (parse b)))

(let ((test-count 0))
    (defun tst (code result &key expect-fail)
        (incf test-count)
        (let ((is-eql (tst-equal (encode (normal (parse code)))
                                 result)))
            (when (if expect-fail is-eql (not is-eql))
                (format t "TEST ~d FAILED" test-count)
                (pprint code)
                (pprint (encode (normal (parse code))))
                (format t "~%")))))

; (\x.x q)
(tst '(:app (:abs x x)
            q)
     'q)

; (\x.\q.x q)
(tst '(:app (:abs x
                  (:abs q x))
            q)
     '(:abs a q))

; (\x.\x.x q)
(tst '(:app (:abs x 
                  (:abs x x))
            q)
     '(:abs a a))

; (\x.\q.(x q) \q.q qq)
(tst '(:app (:app (:abs x 
                        (:abs q
                              (:app x q)))
                  (:abs q q))
            qq)
     'qq)

; ((\x.\q.(x q) \q.\x.q qq) qqq)
(tst '(:app (:app (:app (:abs x
                              (:abs q
                                    (:app x q)))
                        (:abs q
                              (:abs x q)))
                  qq)
            qqq)
     'qq)

;;;;;;

(tst '(:app pi (:abs x x))
     '(:app pi (:abs y y)))

(tst '(:app pi (:abs x x))
     '(:app pi (:abs y x))
     :expect-fail t)

(tst '(:app pi (:abs x x))
     '(:app pi x)
     :expect-fail t)

