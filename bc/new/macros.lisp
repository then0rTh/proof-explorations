(load "utils.lisp")
(load "tst.lisp")
(defpackage "MACROS"
            (:use "COMMON-LISP")
            (:export "EXPAND"))
(in-package macros)


(defvar *macro-list* nil)

(defmethod lookup-macro-fn ((name symbol))
    (cdr (assoc name *macro-list*)))


; macro-list is plist - list of (symbol . function) pairs (TODO: type specifier)
(defmethod expand ((macro-list list) expr)
    (let ((*macro-list* macro-list))
        (expand-inner expr)))


(defmethod expand-inner ((expr symbol))
    expr)

(defmethod expand-inner ((expr list))
    (let* ((op (car expr))
           (args (cdr expr))
           (macro-fn (and (symbolp op) ; macros must be referenced directly
                          (lookup-macro-fn op))))
        (if macro-fn
            (expand-inner (apply macro-fn args))
            (mapcar #'expand-inner expr))))


