(load "utils.lisp")
(load "tst.lisp")
(defpackage "SYNTAX"
            (:use "COMMON-LISP")
            (:export "PIT" "->" "LAM" "CALL"
                     "MACROS"
                     "PI" "LAMBDA"))
(in-package syntax)

; TODO: should the macro names be : (package-agnostic)?
; TODO: dont export functions directly (and rename them to *-macro)
(setf marcros
      (list (cons 'pi #'PIt)
            (cons '-> #'->)
            (cons 'lambda #'lam)
            (cons 'call #'call)))

;;;;;;;;;;;;;
;; PI type ;;

(defun PIt (head &rest rest)
    (if rest
        (PIt-single head
                    (apply #'PIt
                           rest))
        head))

(defun PIt-single (param body)
    `(:app (:app :pi
                 ,(second param))
           (:abs ,(first param)
                 ,body)))


; tests

(tst:deftst tst-PIt #'equal
            :name "syntax:PIt")

(tst-PIt (PIt-single '(T :u) (PIt-single '(x T) 'T))
         '(:app (:app :pi :u)
                (:abs T
                      (:app (:app :pi T)
                            (:abs x
                                  T)))))

(tst-PIt (PIt '(T :u) '(x T) 'T)
         '(:app (:app :pi :u)
                (:abs T
                      (:app (:app :pi T)
                            (:abs x
                                  T)))))



;;;;;;;;;;;;;
;; -> type ;;

(defun -> (&rest rest)
    (apply #'PIt
           (apply #'add-random-names rest)))


(let ((random-name-label "syntax-module-random-name"))

    (defun random-name ()
        (gensym random-name-label))

    (defun random-name-p (x)
        (and (symbolp x)
             (utils:starts-with (symbol-name x)
                                random-name-label))))


(defun add-random-names (head &rest rest)
    ; turn '(a b c) into '((#_G1 a) (#_G2 b) c)
    (if rest
        (cons (list (random-name)
                    head)
              (apply #'add-random-names
                     rest))
        (list head))) ; dont add name to last one - that's body


; tests

(defun equal-modulo-random-names (a b)
    (or (equal a b)
        (and (random-name-p a)
             (random-name-p b))
        (and (listp a)
             (listp b)
             (every #'equal-modulo-random-names a b))))

(tst:deftst tst-> #'equal-modulo-random-names
            :name "syntax:->")

(tst-> (random-name)
       (random-name))

(tst-> (list 'a (random-name))
       (list 'a (random-name)))

(tst-> (-> 'a 'b 'c)
       (PIt `(,(random-name) a)
            `(,(random-name) b)
            'c))

(tst-> (-> 'a 'b)
       `(:app (:app :pi a)
              (:abs ,(random-name)
                    b)))



;;;;;;;;;;;;;;;;;;;;;;;;;;
;; abstraction (lambda) ;;

(defun lam (head &rest rest)
    (if rest
        `(:abs ,head
               ,(apply #'lam
                       rest))
        head))


; tests

(tst:deftst tst-abs #'equal
            :name "syntax:lamb")

(tst-abs (lam 'param 'body)
         '(:abs param body))

(tst-abs (lam 'p1 'p2 'p3 'body)
         '(:abs p1
                (:abs p2
                      (:abs p3
                            body))))



;;;;;;;;;;;;;;;;;
;; application ;;

(defun call (fun arg &rest rest-args)
    (if rest-args
        (apply #'call 
               (cons (call fun arg)
                     rest-args))
        `(:app ,fun ,arg)))


; test

(tst:deftst tst-app #'equal
            :name "syntax:call")

(tst-app (call 'fn 'arg)
         '(:app fn arg))

(tst-app (call 'fn 'a1 'a2 'a3)
         '(:app (:app (:app fn a1) a2) a3))

