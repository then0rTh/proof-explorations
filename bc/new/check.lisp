(load "record.lisp")
(load "lam.lisp")
(load "env.lisp")
(load "tst.lisp")
(load "syntax.lisp")
(defpackage "CHECK"
            (:use "COMMON-LISP" "RECORD" "SYNTAX")
            (:export "typecheck"))
(in-package check)


; TODO: implicit parameters (inferred value) - {} v agde, UNIFIKACE

; TODO: pridat let jako konstrukci jazyka (jmeno, typ, hodnota) - kvuli typu to nemuze byt makro

; TODO: makra (psat v common lispu - jmeno makra je chranene - kontrolovat lambda-list a let pred/pri kompilaci)


(setf const-pi-variabl
      (make-instance 'lam:variabl
                     :name-symbol :pi))

(setf const-u-variabl
      (make-instance 'lam:variabl
                     :name-symbol :u))

(defmethod u+1 ((u lam:expression))
    (unless (is-u u)
        (error "u must be u"))
    (make-instance 'lam:application
                   :operator const-u-variabl
                   :argument u))

(defmethod is-u ((expr lam:expression))
    nil)

(defmethod is-u ((expr lam:variabl))
    (lam:same expr
              const-u-variabl))

; TODO: HTT cast o typech - jak implementovat U1 U2 ... (1.3 nebo zaverecna cast o formalni spec)
; TODO: porovnat s HTT nebo implementaci agdy
; TODO: prozkoumat: forall n: A:U(n) => A:U(n+1) (ale nemusi to tak byt - viz HTT)
; NOTE: mozna misto (:u (:u :u)) rozlisovat succesora a zaklad (:succ-u (:succ-u :u)))
(defmethod is-u ((expr lam:application))
    (and (lam:same (lam:operator expr)
                   const-u-variabl)
         (is-u (lam:argument expr))))


(defrecord pi-type ()
    (param-type lam:expression)
    (pi-abs lam:abstraction))

(defmethod pretty-encode-pi-type ((expr pi-type))
    (list 'PIt (list (lam:encode (lam:param (pi-abs expr)))
                     (lam:encode (param-type expr)))
               (lam:encode (lam:body (pi-abs expr)))))

(defmethod try-parse-pi-type ((expr lam:expression))
    ; expr = (:pi IN \x.OUT)
    ; expr = (:app (:app :pi IN) (:abs x OUT))
    ;                        ^^  ^^^^^^^^^^^^
    ;                 ______/  \  \        /
    ;                /param-type\  \pi-abs/
    (if (and (typep expr
                    'lam:application)
             (typep (lam:operator expr)
                    'lam:application)
             (lam:same (lam:operator (lam:operator expr))
                       const-pi-variabl)
             (typep (lam:argument expr)
                    'lam:abstraction))
        (make-instance 'pi-type
                       :param-type (lam:argument (lam:operator expr))
                       :pi-abs (lam:argument expr))
      nil))


;;;;;;;;;;;;;;;
;; TYPECHECK ;;

(defmethod get-type ((expr lam:variabl) (env env:env))
    (tst:logg (list 'get-type (lam:encode expr)))
    (env:lookup env expr))

(defmethod get-type :around ((expr lam:application) (env env:env))
    (if (try-make-pi-type expr env)
        const-u-variabl
        (call-next-method)))

(defmethod get-type ((expr lam:application) (env env:env))
    (tst:logg (list 'get-type (lam:encode expr)))
    (let* ((op (lam:operator expr))
           (arg (lam:argument expr))
           (op-type (get-type op env)))
        (tst:context (list "op's type must be a PI" (lam:encode expr) (lam:encode op-type))
            (unless (typep op-type 'pi-type)
                (error 'get-type-app1)))
        (tst:context (list "typecheck arg against PI's param-type" (lam:encode expr) (lam:encode op-type))
            (unless (typecheck arg (param-type op-type) env)
                (error 'get-type-app2)))
        (tst:context (list "call PI-abs with arg to get type of application" (lam:encode expr) (lam:encode op-type))
            (lam:beta (pi-abs op-type) arg))))

(defmethod get-type ((expr lam:abstraction) (env env:env))
    (error 'get-type-abs))


(defmethod typecheck :around ((expr lam:expression) (typ lam:expression) (env env:env))
    ; :around is also around :before methods
    (tst:context (list "typecheck" (lam:encode expr) (lam:encode typ))
        (call-next-method)))

(defmethod typecheck :before ((expr lam:expression) (typ lam:expression) (env env:env))
    (unless (is-type typ env)
            (error "internal error: typecheck typ is not type: ~a" (lam:encode typ))))

(defmethod typecheck ((expr lam:variabl) (typ lam:expression) (env env:env))
    (tst:logg (list "typecheck var" (lam:encode expr) (lam:encode typ)))
    (or (lam:same typ
                  (env:lookup env expr))
        (error (make-condition 'type-mismatch
                               :expr expr
                               :expected typ
                               :actual (env:lookup env expr)))))

(defmethod typecheck ((expr lam:application) (typ lam:expression) (env env:env))
    (tst:logg (list "typecheck app" (lam:encode expr) (lam:encode typ)))
    ; TODO: catch get-type errors and return nil
    (lam:same typ
              (get-type expr env)))

(defmethod typecheck ((expr lam:abstraction) (typ lam:expression) (env env:env))
    (tst:logg (list "typecheck abs" (lam:encode expr) (lam:encode typ)))
    (let ((pi-type (try-make-pi-type typ env)))
        (if pi-type
            (typecheck-abstraction expr pi-type env)
          (error "not PI"))))

(defmethod typecheck-abstraction ((expr lam:abstraction) (typ pi-type) (env env:env))
    (let ((env (env:with env (lam:param expr) (param-type typ)))
          (body-type (lam:beta (pi-abs typ) (lam:param expr))))
        (tst:context (list "PI must return a type" (pretty-encode-pi-type typ)) ;TODO:list
            (unless (is-type body-type env)
                (error (make-condition 'pi-body-not-type))))
        (typecheck (lam:body expr)
                   body-type
                   env)))


(defmethod is-type ((expr lam:expression) (env env:env))
    (let ((normal (lam:normal expr)))
        (or (is-u normal)
            (typecheck normal const-u-variabl env))))


(defmethod try-make-pi-type ((expr lam:expression) (env env:env))
    (let ((pit (try-parse-pi-type expr)))
        (unless (is-type (param-type pit) env)
            (error 'pi-param-not-type))
        pit))



;;;;;;;;;;;;;;;;
;; EXCEPTIONS ;;

(define-condition pi-param-not-type (error)
    ())
(define-condition pi-body-not-type (error)
    ())

(define-condition type-mismatch (error)
    ((expr :type lam:expression
           :initarg :expr)
     (expected :type lam:expression
               :initarg :expected)
     (actual :type lam:expression
             :initarg :actual))
    (:report (lambda (er stream)
                 (format stream
                         "Type mismatch in expression:~%  ~a~%Expected type:~%  ~a~%But found:~%  ~a"
                         (lam:encode (slot-value er 'expr))
                         (lam:encode (slot-value er 'expected))
                         (lam:encode (slot-value er 'actual))))))



;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;

(unless (try-parse-pi-type (lam:parse '(:app (:app :pi IN) (:abs x OUT))))
    (print "wrong pi"))

(if (try-parse-pi-type (lam:parse '(:app (:app :qq IN) (:abs x OUT))))
    (print "wrong pi (false positive 1)"))
(if (try-parse-pi-type (lam:parse 'qq))
    (print "wrong pi (false positive 2)"))



;;;;;;;;;;;;;;;;;;;;
;; TEST TYPECHECK ;;

(defun tst-cb (typ val)
    (typecheck (lam:parse val)
               (lam:parse typ)
               (env:empty)))

(tst:deftst tst #'tst-cb
            :name "typecheck")


(tst (PIt '(T :u) '(x T) 'T)
     '(:abs T (:abs x x)))

(tst (PIt '(x T) 'T)
     '(:abs x x)
     :expect-fail 'env:name-not-found) ;T

(tst (PIt '(T :u) '(x T) 'K)
     '(:abs T (:abs x x))
     :expect-fail 'env:name-not-found) ;K

(tst (PIt '(T :u) '(xx T) 'xx)
     '(:abs Tt (:abs xa xa))
     :expect-fail 'type-mismatch) ; PI must return type (xx is not type)
;(TODO: different error - specific type mismatch?)

(tst (PIt '(T :u) '(x T) '(y x) 'x)
     '(:abs T (:abs x (:abs y x)))
     :expect-fail 'type-mismatch) ;PI param must be a type
;(TODO: add context and different error - specific type mismatch?)

(tst (PIt '(T :u)
          (PIt '(K :u)
              (PIt '(x T)
                    (PIt '(y K)
                         'T))))
     '(:abs T
            (:abs K
                (:abs x
                    (:abs y 
                          x)))))

(tst (PIt '(T :u)
          (PIt '(K :u)
              (PIt '(x T)
                  (PIt '(y K)
                       'T))))
     '(:abs x 
            (:abs y 
                  y))
     :expect-fail 'type-mismatch) ; expected 2 more abstractions
;(TODO: different error - specific type mismatch?)


(tst (PIt '(T :u)
          (PIt '(K :u)
              (PIt '(x T)
                    (PIt '(y K)
                         'K))))
     '(:abs T
            (:abs K
                (:abs x
                    (:abs y 
                          y)))))



; proofs

;cases : {A B C : Set} -> Either A B -> (A -> C) -> (B -> C) -> C
;cases (left a) ab cb = ab a
;cases (right b) ab cb = cb b

;--A implies A
(tst (PIt '(A :u) '(x A) 'A)
     (lam 'A 'x 'x))

;--If A then (B implies A).
(tst (PIt '(A :u) '(B :u) (-> 'A 'B 'A))
     (lam 'A 'B 'x 'y 'x))

;--If (A and true) then (A or false).
;ex2 : {A : Set} -> Pair A Unit -> Either A Void
;ex2 (pair a u) = left a

;--If A implies (B implies C), then (A and B) implies C.
;ex3 : {A B C : Set} -> (A -> B -> C) -> Pair A B -> C
;ex3 fn (pair a b) = fn a b

;--If A and (B or C), then either (A and B) or (A and C).
;ex4 : {A B C : Set} -> Pair A (Either B C) -> Either (Pair A B) (Pair A C)
;ex4 (pair a (left b)) = left (pair a b)
;ex4 (pair a (right c)) = right (pair a c)

;--If A implies C and B implies D, then (A and B) implies (C and D).
;ex5 : {A B C D : Set} -> Pair (A -> C) (B -> D) -> Pair A B -> Pair C D
;ex5 (pair ac bd) (pair a b) = pair (ac a) (bd b)

;-- the law of the excluded middle (“either P or not P”)
;law : {P : Set} -> Not (Not (Either P (Not P)))
;law {P} x = x (right notP)
;  where
;  notP : Not P
;  notP y = x (left y)cases : {A B C : Set} -> Either A B -> (A -> C) -> (B -> C) -> C

