(defpackage "RECORD"
            (:use "COMMON-LISP")
            (:export "DEFRECORD"))
(in-package record)


(defun defrecord-make-field (field)
    (let ((name (first field))
          (typ (second field)))
        (list name
              :type typ
              :reader name
              :initarg (intern (symbol-name name) "KEYWORD"))))


(defmacro defrecord (name parent &rest fields)
    (list 'defclass name parent
          (mapcar #'defrecord-make-field fields)))

