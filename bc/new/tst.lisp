(defpackage "TST"
            (:use "COMMON-LISP")
            (:export "DEFTST" "LOGG" "CONTEXT"))
(in-package tst)


(defvar *log-list* nil)

(defun logg (msg)
    (setf *log-list* 
          (cons msg *log-list*)))

(defun print-logs ()
    (let ((*print-level* 4))
        (loop for msg in (reverse *log-list*)
          do (pprint msg))))


(defvar *log-stack* nil)
(defvar *highest-log-stack* nil)

(defmacro context (msg &body body)
    `(let ((*log-stack* (cons ,msg *log-stack*)))
         (handler-bind ((error (lambda (er)
                                   (unless *highest-log-stack*
                                       (setf *highest-log-stack* *log-stack*)))))
             ,@body)))

(defun print-stack ()
    (let ((*print-level* 4))
        (if *highest-log-stack*
            (print-stack-inner *highest-log-stack* 3)
            (format t "~%  empty"))))

(defun print-stack-inner (stack depth)
    (when (and stack
               (> depth 0))
        (print-stack-inner (cdr stack)
                           (1- depth))
        (pprint (car stack))))


(defmacro deftst (fn-name test-callback &key (debug nil) (name ""))
    `(let ((test-count 0)
           (run-test ,test-callback)
           (run-debug ,debug)
           (test-name ,name))
         (defun ,fn-name (a b &key (expect-fail nil) (log nil))
             (incf test-count)
             (let* ((*log-list* nil)
                    (*log-stack* nil)
                    (*highest-log-stack* nil)
                    (test-error nil)
                    (test-result (handler-case (funcall run-test a b)
                                       (error (er)
                                          (logg (format nil "ERROR: ~a" er))
                                          (setf test-error er)
                                          nil)))
                    (is-test-ok (cond ((eql expect-fail t) (not test-result))
                                      ((eql expect-fail nil) test-result)
                                      ((typep expect-fail 'symbol) (and test-error
                                                                        (typep test-error expect-fail)))
                                      (t (error "internal error: invalid :expect-fail")))))
                 (unless is-test-ok
                     (format t "~%~%TEST ~a ~d FAILED" test-name test-count)
                     (format t "~%~%Stack (most recent last):")
                     (print-stack)
                     (when test-error
                         (format t "~%~%ERROR: ~a" test-error))
                     (format t "~%~%Input:")
                     (pprint a)
                     (pprint b)
                     (when run-debug
                         (format t "~%~%Debug:")
                         (pprint (funcall run-debug a b)))
                     (when log
                         (format t "~%~%Logs:")
                         (print-logs))
                     (format t "~%"))))))

