(load "record.lisp")
(defpackage "ENV"
            (:use "COMMON-LISP" "RECORD")
            (:export "ENV" "WITH" "LOOKUP" "EMPTY" "NAME-NOT-FOUND"))
(in-package env)

(defrecord env ()
    (env-list list))

(defun empty ()
    (make-instance 'env
                   :env-list nil))

(defmethod with ((env env) (name lam:variabl) (value lam:expression))
    (make-instance 'env
                   :env-list (cons (cons name value)
                                   (env-list env))))

(defmethod lookup ((env env) (name lam:variabl))
    (lookup-inner (env-list env) name))

(defmethod lookup-inner ((env-list (eql nil)) (name lam:variabl))
    (let ((er (make-condition 'name-not-found)))
        (setf (slot-value er 'name) name)
        (error er)))

(defmethod lookup-inner ((env-list cons) (name lam:variabl))
    (if (lam:same name (caar env-list))
        (cdar env-list)
        (lookup-inner (cdr env-list) name)))


(define-condition name-not-found (error)
    ((name :type 'lam:variabl))
    (:report (lambda (er stream)
                 (format stream
                         "[env] Name not found: ~a"
                         (lam:encode (slot-value er 'name))))))

