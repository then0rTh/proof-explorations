(defpackage "UTILS"
            (:use "COMMON-LISP")
            (:export "STARTS-WITH"))
(in-package utils)


(defun starts-with (whole start)
    (equal start
           (subseq whole
                   0
                   (length start))))

